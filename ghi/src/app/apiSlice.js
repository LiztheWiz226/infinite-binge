import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const movieApi = createApi({
  reducerPath: "movieApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
  }),
  endpoints: (builder) => ({
    getAllMovies: builder.query({
      query: () => "api/movies/popular",
    }),
    getUpcomingMovies: builder.query({
      query: () => "api/movies/upcoming",
    }),
    getTopRatedMovies: builder.query({
      query: () => "api/movies/top_rated",
    }),
    getNowPlayingMovies: builder.query({
      query: () => "api/movies/now_playing",
    }),
    getMovieById: builder.query({
      query: (movie_id) => `/api/movies/${movie_id}`,
    }),
    getMovieVideo: builder.query({
      query: (movie_id) => `/api/movies/${movie_id}/videos`,
    }),
    getMoviesByKeyword: builder.query({
      query: (keyword) => `/api/movies/search/${keyword}`,
    }),
    getAccount: builder.query({
      query: () => ({
        url: `/token`,
        credentials: "include",
      }),
      transformResponse: (response) => response?.account || null,
      providesTags: ["Account"],
    }),
    signup: builder.mutation({
      query: ({ username, email, password }) => {
        const body = JSON.stringify({ username, email, password });
        return {
          url: `/api/accounts`,
          method: "POST",
          body,
          credentials: "include",
          headers: {
            "Content-Type": "application/json",
          },
        };
      },
      invalidatesTags: ["Account"],
    }),
    login: builder.mutation({
      query: ({ username, password }) => {
        const body = new FormData();
        body.append("username", username);
        body.append("password", password);
        return {
          url: `/token`,
          method: "POST",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: `/token`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account"],
    }),
    getFavorites: builder.query({
      query: () => ({
        url: "/api/favorites",
        credentials: "include",
      }),
      providesTags: ["Favorites"],
    }),
    addFavorite: builder.mutation({
      query: ({ movie_id }) => ({
        url: `/api/favorites`,
        method: "POST",
        body: { movie_id: movie_id },
        credentials: "include",
      }),
      invalidatesTags: ["Favorites"],
    }),
    deleteFavorite: builder.mutation({
      query: ({ movie_id }) => ({
        url: `/api/favorites/${movie_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Favorites"],
    }),
  }),
});

export const {
  useDeleteFavoriteMutation,
  useAddFavoriteMutation,
  useGetAllMoviesQuery,
  useGetMovieByIdQuery,
  useGetAccountQuery,
  useLogoutMutation,
  useLoginMutation,
  useSignupMutation,
  useGetMoviesByKeywordQuery,
  useGetFavoritesQuery,
  useGetUpcomingMoviesQuery,
  useGetNowPlayingMoviesQuery,
  useGetTopRatedMoviesQuery,
  useGetMovieVideoQuery
} = movieApi;
