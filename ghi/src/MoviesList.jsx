import React, { useState } from "react";
import {
  useGetAllMoviesQuery,
  useGetMoviesByKeywordQuery,
} from "./app/apiSlice";
import MovieCard from "./MovieCard";
import Search from "./Search";
import { useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import "./index.css";
import logoIcon from './logoIcon.png';

const MoviesList = () => {
  const [searchCriteria, setSearchCriteria] = useState('');
  const [displayCount, setDisplayCount] = useState(20);
  const navigate = useNavigate();

  const { data, isLoading, isError } = useGetAllMoviesQuery();
  const {
    data: searchData,
    isLoading: searchLoading,
    isError: searchError,
  } = useGetMoviesByKeywordQuery(searchCriteria);

  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const paramKeyword = params.get("keyword");

  if (isLoading || searchLoading) return <div>Loading...</div>;

  const movieResults = () => {
    if (!searchCriteria) {
      if (paramKeyword) {
        setSearchCriteria(paramKeyword);
      }
      return data?.movies;
    }
    if (searchError) return [];
    return searchData?.movies || [];
  };
  const handleReset = () => {
    setSearchCriteria("");
    navigate("/search");
  };

  const loadMore = () => {
    setDisplayCount((prevCount) => prevCount + 4);
  };

  return (
    <div style={{ minHeight: "100vh" }}>
      <div className="logo" style={{ marginBottom: "40px", marginTop: "90px" }}>
        <img
          className="d-block mx-auto "
          src={logoIcon}
          alt=""
          width="200"
        />
      </div>
      <div className="row my-3">
        <div className="col-12">
          <div className="d-flex justify-content-center">
            <Search
              searchCriteria={searchCriteria}
              setSearchCriteria={setSearchCriteria}
            />
            {searchCriteria && (
              <button
                className="btn btn-sm btn-danger ml-2"
                onClick={handleReset}
              >
                Reset
              </button>
            )}
          </div>
        </div>
        <div className="col-12">
          {paramKeyword ? (
            <h2 className="search-results">
              Movie results for "{paramKeyword}"
            </h2>
          ) : null}
          {searchCriteria && !paramKeyword ? (
            <h2 className="search-results">
              movie results for "{searchCriteria}"
            </h2>
          ) : null}
          {searchError || searchData.movies === null ? (
            <h2 className="search-results">
              Your search did not return any results. Try again!
            </h2>
          ) : null}
          {isError ? (
            <h2 className="search-results">
              Oops! An error occurred. Please try again later or contact IT for
              help
            </h2>
          ) : null}
        </div>
        <div className="col-12">
          <div className="movie-section">
            <div className="row row-cols-1 row-cols-md-4">
              {movieResults()
                ?.slice(0, displayCount)
                .map((movie) => (
                  <div key={movie.id} className="col mb-3">
                    <MovieCard movie={movie} />
                  </div>
                ))}
            </div>
          </div>
          {movieResults().length > displayCount && (
            <div className="button-container d-flex justify-content-center">
              <button className="load-more-button" onClick={loadMore}>
                Load More
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default MoviesList;
