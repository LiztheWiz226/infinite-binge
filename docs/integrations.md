The application will need to get the following kinds of data from third-party sources:

Movie data from TMBD API https://api.themoviedb.org/


After creating an account on TMDB, find the API key for your account. Create a keys.py file in the api directory and add it to .gitignore. Then enter:

`api_key= "{API Key}"`
