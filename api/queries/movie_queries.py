import requests
import os
from queries.client import MongoRepo

api_key = os.environ.get("MOVIE_API_KEY")


class MovieQueries(MongoRepo):
    collection_name = "movies"

    def get_all(self):
        url = "https://api.themoviedb.org/3/movie/popular?language=en-US"
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}",
        }
        response = requests.get(url, headers=headers).json()
        movies = []
        for num in range(0, 20):
            movie_data = {
                "id": response["results"][num]["id"],
                "original_title": response["results"][num]["original_title"],
                "overview": response["results"][num]["overview"],
                "poster_path": response["results"][num]["poster_path"],
                "vote_average": response["results"][num]["vote_average"],
            }
            movies.append(movie_data)
        return movies

    def get_by_search(self, keyword: str):
        movies = []
        for pg in range(1, 6):
            url = (
                f"https://api.themoviedb.org/3/search/"
                f"movie?query={keyword}&include_adult=false"
                f"&language=en-US&page={pg}"
            )
            headers = {
                "accept": "application/json",
                "Authorization": f"Bearer {api_key}",
            }
            response = requests.get(url, headers=headers).json()
            total_results = response["total_results"]
            if total_results == 0:
                return
            for num in range(0, 20):
                movie_data = {
                    "id": response["results"][num]["id"],
                    "original_title": response["results"][num][
                        "original_title"
                    ],
                    "overview": response["results"][num]["overview"],
                    "poster_path": response["results"][num]["poster_path"],
                    "vote_average": response["results"][num]["vote_average"],
                }
                movies.append(movie_data)
                if len(movies) == total_results:
                    return movies
        return movies

    def get_by_id(self, movie_id: str):
        url = f"https://api.themoviedb.org/3/movie/{movie_id}"
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}",
        }
        response = requests.get(url, headers=headers)
        return response.json()

    def get_video(self, movie_id: str):
        videos_url = f"https://api.themoviedb.org/3/movie/{movie_id}/videos"
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}",
        }

        videos_response = requests.get(videos_url, headers=headers)
        videos_data = videos_response.json()
        if videos_data["results"] == []:
            return {"name": "No trailer available"}

        for video in videos_data["results"]:
            if video["type"] == "Trailer":
                return {
                    "id": videos_data["id"],
                    "key": video["key"],
                    "site": video["site"],
                    "type": video["type"],
                    "name": video["name"],
                }

        return {"name": "No trailer available"}

    def get_upcoming_movies(self):
        url = "https://api.themoviedb.org/3/movie/upcoming?language=en-US"
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}",
        }
        response = requests.get(url, headers=headers).json()
        movies = []
        for num in range(0, 20):
            movie_data = {
                "id": response["results"][num]["id"],
                "original_title": response["results"][num]["original_title"],
                "overview": response["results"][num]["overview"],
                "poster_path": response["results"][num]["poster_path"],
                "vote_average": response["results"][num]["vote_average"],
            }
            movies.append(movie_data)
        return movies

    def get_now_playing(self):
        url = "https://api.themoviedb.org/3/movie/now_playing?language=en-US"
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}",
        }
        response = requests.get(url, headers=headers).json()
        movies = []
        for num in range(0, 20):
            movie_data = {
                "id": response["results"][num]["id"],
                "original_title": response["results"][num]["original_title"],
                "overview": response["results"][num]["overview"],
                "poster_path": response["results"][num]["poster_path"],
                "vote_average": response["results"][num]["vote_average"],
            }
            movies.append(movie_data)
        return movies

    def get_top_rated(self):
        url = (
            "https://api.themoviedb.org/3/movie/"
            "top_rated?language=en-US&page=1"
        )
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}",
        }
        response = requests.get(url, headers=headers).json()
        movies = []
        for num in range(0, 20):
            movie_data = {
                "id": response["results"][num]["id"],
                "original_title": response["results"][num]["original_title"],
                "overview": response["results"][num]["overview"],
                "poster_path": response["results"][num]["poster_path"],
                "vote_average": response["results"][num]["vote_average"],
            }
            movies.append(movie_data)
        return movies
