# 5/15
I set up the yaml file. I found that when listing the enviroment variables with "-" makes a difference when connecting to mongo & not using "-".

# 5/22
Today we worked on front end & re-discussed our wire framework. We talked about design & decided on how we wanted our user to experience our application.

# 5/25
We worked on parsing through the data in order to connect our back end to our front end. It was exciting to actually see our API showing the information we needed.

# 5/30
We wrote unit tests for the back end to ensure that our queries & routers are working in the way we intended them to.

# 6/1
We worked on trying to implement a carosuel. We were able to get the carosuel to work, however, the data was not structured the way we wanted.

# 6/2
Today Liz & I worked on tightening up our application. Some of the routes were not redirecting the user as we expected. We made some small changes to the Login & Signup JSX files that allowed for a smoother user experience. We also decided to work on handling an error when the user tried to login with incorrect and/or invalid credientials.

# 6/3
Liz & I worked on an error concerning hitting the API. We found that when we searched for the movie wasp or frown we would get an Index Error. This is because the return for the movie wasp/frown is out of range. Normally, the API would return 20 movies per page. Usually, each search result would have thousands of pages. However, these movies do not even have 20 movies total. We ended up adding a line in movie_queries so that if the total_results equal 0 then the query would break out before trying to return said data. We then added some error handling on the front end in the case the query did not return anything at all.

# 6/8
Today Liz & I worked on a bug from searching for a movie from the home page / searching from the search page. We were having issues with the useSelect in the front end. Also, we struggled with the way that the MovieList & Home JSX files were using the Search component. We ended up using useState instead of the useSelect & renamed variables in order to gain clarity between each files data transfer. We ended up using useLocation.

# 6/9
Liz, Sam & I worked on adding the trailer to MovieDetail page & worked through some little errors. We will be working on cleaning up code today & working on stretch goals next week.
