import React from "react";

const Footer = () => {
    return (
        <footer style={{ backgroundColor: "black", color: "#09f0a0", textAlign: "center", padding: "20px", position: "relative", width: "100%", fontFamily: 'Swiss721Reg' }}>
            CREATED BY THE INFINITE LOOPERS: LIZ W, CHAD N, KAYSI M, SAM D, HUMZA Q
        </footer>
    );
};

export default Footer;
