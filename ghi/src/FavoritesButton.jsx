import { useState, useEffect } from "react";
import {
    useAddFavoriteMutation,
    useDeleteFavoriteMutation,
    useGetAccountQuery,
    useGetFavoritesQuery,
} from "./app/apiSlice";
import { useNavigate } from "react-router-dom";
import "./index.css";

const FavoriteButton = ({ movieId }) => {
    const { data: account } = useGetAccountQuery();
    const [addFavorite] = useAddFavoriteMutation();
    const [deleteFavorite] = useDeleteFavoriteMutation();

    const navigate = useNavigate();

    const [isFavorited, setIsFavorited] = useState(false);
    const { data: favorites } = useGetFavoritesQuery(account?.id, {
        skip: !account,
    });

    useEffect(() => {
        if (favorites) {
            setIsFavorited(
                favorites.some(
                    (favorite) => Number(favorite.movie_id) === Number(movieId)
                )
            );
        }
    }, [favorites, movieId]);

    const handleFavoriteClick = async () => {
        if (!account) {
            return navigate("/login");
        }

        try {
            if (isFavorited) {
                await deleteFavorite({ movie_id: movieId }).unwrap();
            } else {
                await addFavorite({ movie_id: movieId }).unwrap();
            }
            setIsFavorited(!isFavorited);
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <button
            className={`btn ${isFavorited ? "btn-unbinge" : "btn-binge"}`}
            onClick={handleFavoriteClick}
        >
            {isFavorited ? "Unbinge" : "Binge"}
        </button>
    );
};

export default FavoriteButton;
