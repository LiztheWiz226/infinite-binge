import React from "react";
import { Link } from "react-router-dom";
import { useGetMovieByIdQuery } from "./app/apiSlice";
import FavoriteButton from "./FavoritesButton";

const MovieCard = ({ movie }) => {
  const posterUrl = "https://image.tmdb.org/t/p/original";
  const maxSummaryLength = 75;

  const shouldShowMoreLink =
    movie.overview && movie.overview.length > maxSummaryLength;

  const isLoading = useGetMovieByIdQuery(movie.id).isLoading;

  if (isLoading) return <div>Loading...</div>;

  return (
    <div className="card mb-3 movie-card-container text-center">
      <div className="card-body bg-black">
        <div className="shadow p-0 mb-0 bg-black rounded">
          <Link to={`/movies/${movie.id}`} className="card-link">
            <h4 className="card-title">
              {movie.original_title}
            </h4>
          </Link>
          {movie.poster_path && (
            <Link to={`/movies/${movie.id}`} className="card-link">
              <img
                src={`${posterUrl}${movie.poster_path}`}
                alt={`Poster for ${movie.original_title}`}
                className="card-img-top"
              />
            </Link>
          )}
          <div className="card-text d-flex flex-column align-items-center">
            <p>
              {shouldShowMoreLink
                ? movie.overview.substring(0, maxSummaryLength) + "..."
                : movie.overview}
              {shouldShowMoreLink && (
                <Link to={`/movies/${movie.id}`} className="card-link">
                  More
                </Link>
              )}
            </p>
            <div className="d-flex justify-content-center">
              <FavoriteButton movieId={movie.id} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MovieCard;
