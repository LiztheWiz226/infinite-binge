from pydantic import BaseModel
from typing import List


class HttpError(BaseModel):
    error_message: str


class Video(BaseModel):
    id: int
    key: str
    name: str
    site: str
    type: str


class Genre(BaseModel):
    id: int
    name: str


class MoviesOut(BaseModel):
    id: int
    original_title: str
    overview: str
    poster_path: str | None = None
    vote_average: int
    genres: List[Genre] | None = None
    videos: List[Video] | None = None


class MovieList(BaseModel):
    movies: List[MoviesOut] | None = None
