from fastapi import APIRouter, Depends
from models.movies import MovieList
from queries.movie_queries import MovieQueries


router = APIRouter()


@router.get("/api/movies/popular", response_model=MovieList)
def get_popular_movies(repo: MovieQueries = Depends()):
    return {"movies": repo.get_all()}


@router.get("/api/movies/upcoming", response_model=MovieList)
def get_upcoming_movies(repo: MovieQueries = Depends()):
    return {"movies": repo.get_upcoming_movies()}


@router.get("/api/movies/now_playing", response_model=MovieList)
def get_now_playing_movies(repo: MovieQueries = Depends()):
    return {"movies": repo.get_now_playing()}


@router.get("/api/movies/top_rated", response_model=MovieList)
def get_top_rated_movies(repo: MovieQueries = Depends()):
    return {"movies": repo.get_top_rated()}


@router.get("/api/movies/search/{keyword}", response_model=MovieList)
def get_searched_movies(keyword: str, repo: MovieQueries = Depends()):
    return {"movies": repo.get_by_search(keyword)}


@router.get("/api/movies/{movie_id}")
def get_movie_details(movie_id: str, repo: MovieQueries = Depends()):
    return {"movie": repo.get_by_id(movie_id)}


@router.get("/api/movies/{movie_id}/videos")
def get_movie_video(movie_id: str, repo: MovieQueries = Depends()):
    return {"movie": repo.get_video(movie_id)}
