import React, { useState, useEffect } from "react";
import {
  useGetTopRatedMoviesQuery,
  useGetNowPlayingMoviesQuery,
  useGetUpcomingMoviesQuery,
} from "./app/apiSlice";
import MovieCard from "./MovieCard";
import Search from "./Search";
import "./index.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Home = () => {
  const [displayCounts, setDisplayCounts] = useState({
    topRated: 4,
    nowPlaying: 4,
    upcoming: 4,
  });
  const [shuffledTopRated, setShuffledTopRated] = useState([]);
  const [shuffledNowPlaying, setShuffledNowPlaying] = useState([]);
  const [shuffledUpcoming, setShuffledUpcoming] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const { data: topRatedMovies } = useGetTopRatedMoviesQuery();
  const { data: nowPlayingMovies } = useGetNowPlayingMoviesQuery();
  const { data: upcomingMovies } = useGetUpcomingMoviesQuery();

  useEffect(() => {
    if (topRatedMovies) {
      const shuffledTopRatedMovies = shuffleArray(topRatedMovies?.movies || []);
      setShuffledTopRated(shuffledTopRatedMovies);
      setDisplayCounts((prevCounts) => ({
        ...prevCounts,
        topRated: Math.min(4, shuffledTopRatedMovies.length),
      }));
    }

    if (nowPlayingMovies) {
      const shuffledNowPlayingMovies = shuffleArray(nowPlayingMovies?.movies || []);
      setShuffledNowPlaying(shuffledNowPlayingMovies);
      setDisplayCounts((prevCounts) => ({
        ...prevCounts,
        nowPlaying: Math.min(4, shuffledNowPlayingMovies.length),
      }));
    }

    if (upcomingMovies) {
      const shuffledUpcomingMovies = shuffleArray(upcomingMovies?.movies || []);
      setShuffledUpcoming(shuffledUpcomingMovies);
      setDisplayCounts((prevCounts) => ({
        ...prevCounts,
        upcoming: Math.min(4, shuffledUpcomingMovies.length),
      }));
    }
  }, [topRatedMovies, nowPlayingMovies, upcomingMovies]);

  const shuffleArray = (array) => {
    const shuffledArray = [...array];
    for (let i = shuffledArray.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
    }
    return shuffledArray;
  };

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
  };

  return (
    <div>
      <div className="logo" style={{ paddingTop: "10px", paddingBottom: "0px" }}>
        <img
          className="d-block mx-auto logo scale-animation"
          src="/LOGO_Text_Only.png"
          alt=""
          width="700"
        />
      </div>
      <div className="search-bar-container" style={{ paddingTop: "18px", paddingBottom: "5px" }}>
        <Search searchQuery={searchQuery} setSearchQuery={setSearchQuery} />
      </div>
      <div className="section-heading">
        <h1 className="section-title">TOP RATED</h1>
      </div>
      {!topRatedMovies ? (
        <p>Loading...</p>
      ) : (
        <Slider {...settings}>
          {shuffledTopRated.map((movie) => (
            <div key={movie.id}>
              <MovieCard movie={movie} />
            </div>
          ))}
        </Slider>
      )}
      <div className="section-heading">
        <h1 className="section-title">NOW PLAYING</h1>
      </div>
      {!nowPlayingMovies ? (
        <p>Loading...</p>
      ) : (
        <Slider {...settings}>
          {shuffledNowPlaying.map((movie) => (
            <div key={movie.id}>
              <MovieCard movie={movie} />
            </div>
          ))}
        </Slider>
      )}
      <div className="section-heading">
        <h1 className="section-title">UPCOMING</h1>
      </div>
      {!upcomingMovies ? (
        <p>Loading...</p>
      ) : (
        <Slider {...settings}>
          {shuffledUpcoming.map((movie) => (
            <div key={movie.id}>
              <MovieCard movie={movie} />
            </div>
          ))}
        </Slider>
      )}
    </div>
  );
};

export default Home;
