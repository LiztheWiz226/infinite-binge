import React from "react";
import { useGetFavoritesQuery } from "./app/apiSlice";
import FavoriteCard from './FavoriteCard';
import logoIcon from "./logoIcon.png";

const Favorites = () => {
    const { data, isLoading } = useGetFavoritesQuery();

    if (isLoading) return <div>Loading...</div>;

    return (
        <div style={{ minHeight: "100vh" }}>
            <div className="logo" style={{ marginBottom: "0px", marginTop: "10px" }}>
                <img
                    className="d-block mx-auto logo"
                    src="/LogoIcon.png"
                    alt=""
                    width="200"
                />
            </div>
            <h1 className="section-title text-center" style={{ marginTop: "-70px" }}>Binge List</h1>
            <div className="movie-section">
                <div className="row row-cols-1 row-cols-md-4">
                    {data?.map(favorite => (
                        <div key={favorite.movie_id} className="col mb-3">
                            <FavoriteCard favorite={favorite} />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Favorites;
