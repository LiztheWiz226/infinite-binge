import React from "react";
import MovieCard from "./MovieCard";

const MovieCarousel = ({ movies, displayCount, updateDisplayCount }) => {
  const handleClickNext = () => {
    updateDisplayCount(displayCount + 4);
  };

  const handleClickPrev = () => {
    updateDisplayCount(Math.max(0, displayCount - 4));
  };

  return (
    <div className="carousel-container">
      <div className="movie-section">
        {movies.slice(0, displayCount).map((movie) => (
          <MovieCard key={movie.id} movie={movie} />
        ))}
        <div className="button-container">
          <button
            className="carousel-button"
            onClick={handleClickPrev}
            style={{
              backgroundColor: "#09f0a0",
              borderColor: "#09f0a0",
              color: "black",
              backgroundColor: "#09f0a0 !important",
              borderColor: "#09f0a0 !important",
              color: "black !important",
            }}
          >
            &lt; Previous
          </button>
          <button
            className="carousel-button"
            onClick={handleClickNext}
            style={{
              backgroundColor: "#09f0a0",
              borderColor: "#09f0a0",
              color: "black",
              backgroundColor: "#09f0a0 !important",
              borderColor: "#09f0a0 !important",
              color: "black !important",
            }}
          >
            Next &gt;
          </button>
        </div>
      </div>
    </div>
  );
};

export default MovieCarousel;
