## Journals


Humza Qureshi
# 05/16
Today we worked on user authentication. We modified our models, queries, and routers in order for our user authentication to efficently return without any recurring errors. We ran into a conflict with our username and email authentication methods, but resolved it by mofifying our queries and creating a client.py file with the assigned variable.

# 5/18
Spent time revising wireframe after touching up on most of our backend. Began preparation to start frontend within the upcoming days.


# 05/22
Today we began our frontend, focusing mostly on redux. We were able to begin on our List page, App.js, index, and app directory for sclices. We implemented our React changes to backend, and also synced it across our js files. We had a few complications with our npm install, as react-dom's import was failing. However, we were able to successfully load the react page after applying the correct routers to our packages.

# 05/26
We started on our unit tests and were able to correctly implement the tests to our routers. In doing so, we also achieved progress in getting the tests to work. At this point, we were in a good spot, so we proceeded by planning the upcoming week by diving into our wireframe and discussing what was left.


# 05/30
After completing backend and frontend authentication, we worked on our tests, to ensure that our routers for both, favorites and movie_queries, were running efficiently. The process was overall successful, however, we came across a few errors in regards to our add_favorite(POST) request, where, the system was catching an error in our object list. It was resolved in removal of the list, as well as the indexes, for the code to be readable by the test.


# 06/1
Today we attempted the carousel for our project, we came across some problems, so we decided to put it on hold while we work on some other conflicts. We were able to improve our search method, as well as the results.

# 06/2
Our logout feature was improved, we also implemented some css in order for the alignment to look much better. We also utilized the css to make the colors stand out a beat better.

# 06/3
Focused more on the README file, went over bugs and errors throughout our code. Split up while focusing on different parts of the project in order for our page to work efficiently.

# 06/8
Focused heavily on the navbar, and improving the feature in which it disappears whilst the user scrolls down. Was able to implement a sticky css/bootstrap that resolved the conflict. Unfortunately, came across the issue of having to hard refresh in order to actually see the changes worked.


# 06/9
Project submission, debugged errors, removed unwanted code, rearranged formatting to make the website look more fitting.
