import { useState } from "react"
import { useNavigate } from "react-router-dom";

const Search = ({ setSearchCriteria, setSearchQuery }) => {
  const [input, setInput] = useState("")
  const navigate = useNavigate();

  const handleChange = (e) => {
    const value = e.target.value
    setInput(value);

  }
  const handleSubmit = (e) => {
    e.preventDefault();
    if (setSearchQuery) {
      navigate(`/search?keyword=${input}`)
    }
    if (setSearchCriteria) {
      setSearchCriteria(input)
    }
    setInput("")
  };

  return (
    <form className="d-flex justify-content-center align-items-center" onSubmit={handleSubmit}>
      <div className="input-group">
        <input
          value={input}
          onChange={handleChange}
          className="form-control form-control-lg search-input"
          type="text"
          placeholder="Search movies here.."
        />
        <button className="btn btn-lg btn-primary" type="submit">
          Search
        </button>{"    "}
      </div>
    </form>
  );
};

export default Search;
