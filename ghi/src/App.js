import "./App.css";
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Nav from "./Nav";
import MoviesList from "./MoviesList";
import Login from "./Login";
import Home from "./Home";
import SignUp from "./SignUp";
import MovieDetails from "./MovieDetails";
import Favorites from "./Favorites";
import Footer from "./Footer";


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <BrowserRouter basename={basename}>
      <div className="bg-navy">
        <Nav />
        <Routes>
          <Route path="/home" element={<Home />} />
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/search" element={<MoviesList />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="/movies/:movie_id" element={<MovieDetails />} />
          <Route path="/bingelist" element={<Favorites />} />
          <Route path="/Logout" element={<Home />} />
        </Routes>
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
